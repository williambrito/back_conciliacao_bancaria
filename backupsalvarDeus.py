from flask import Flask, make_response, render_template
from ofxparse import OfxParser
import psycopg2

app = Flask(__name__)

@app.route('/inserirofx')
def nome_da_funcao():
    transactions = criacaoofx('BB.ofx')
    executequery(transactions)
    
    return "transactions"

if __name__ == '__main__':
    app.run()

def executequery (transactions):
    conn = psycopg2.connect(
            host="192.168.1.3",
            database="homologacao_nfe_old",
            user="taxationmind",
            password="",
            port="5432"
            )
    cursor = conn.cursor()
    for transaction in transactions:
        try:            
            cursor.execute(
                "INSERT INTO conciliacao.tb_valores (data, movimentacao, debito, credito, n_doc, bank_id, account_id, cliente_id, razao_social) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
                (
                transaction['data'],
                transaction['movimentacao'],
                transaction['debito'],
                transaction['credito'],
                transaction['n_doc'],
                transaction['bank_id'],
                transaction['account_id'].replace('-',''),
                transaction['cliente_id'],
                transaction['razao_social'],
                )
            )
            conn.commit()
        except(Exception) as error:
            print('deu pau')
            print(error)

def criacaoofx (file):
    with open(file, 'rb') as bb_ofx:
        ofx = OfxParser.parse(bb_ofx)
    transactions = []
    cliente_id = input("Digite o id do cliente:")
    razao_social = input("Digite a razão social:")
    for account in ofx.accounts:
        for transaction in account.statement.transactions:
            if transaction.amount < 0:
                debito = abs(transaction.amount)
                credito = 0.00
            else:
                debito = 0.00
                credito = max(transaction.amount, 0)
            transactions.append({
                'data': transaction.date.date(),
                'movimentacao': transaction.memo,
                'debito': debito,
                'credito': credito,
                'n_doc': transaction.checknum,
                'banco': account.institution,
                'bank_id': account.routing_number,
                'account_id': account.account_id,
                'cliente_id': cliente_id,
                'razao_social': razao_social,
            })
    return transactions