cursor = conn.cursor()

for transaction in transactions:
    cursor.execute(
        "INSERT INTO conciliacao.tb_valores (data, movimentacao, debito, credito, n_doc, bank_id, account_id, cliente_id, razao_social) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
        (
            transaction['data'],
            transaction['movimentacao'],
            transaction['debito'],
            transaction['credito'],
            transaction['n_doc'],
            transaction['bank_id'],
            transaction['account_id'].replace('-',''),
            transaction['cliente_id'],
            transaction['razao_social'],
        )
    )

conn.commit()
cursor.close()
conn.close()
