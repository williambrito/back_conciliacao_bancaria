import os
from ofxparse import OfxParser
import psycopg2

    
conn = psycopg2.connect(
    host="192.168.1.3",
    database="homologacao_nfe_old",
    user="taxationmind",
    password="",
    port="5432"
)

nome_arquivo_ofx = 'BB.ofx'

with open(nome_arquivo_ofx, 'rb') as bb_ofx:
    ofx = OfxParser.parse(bb_ofx)

transactions = []

cliente_id = input("Digite o id do cliente:")
razao_social = input("Digite a razão social:")

for account in ofx.accounts:
    for transaction in account.statement.transactions:
        if transaction.amount < 0:
            debito = abs(transaction.amount)
            credito = 0.00
        else:
            debito = 0.00
            credito = max(transaction.amount, 0)
        transactions.append({
            'data': transaction.date.date(),
            'movimentacao': transaction.memo,
            'debito': debito,
            'credito': credito,
            'n_doc': transaction.checknum,
            'banco': account.institution,
            'bank_id': account.routing_number,
            'account_id': account.account_id,
            'cliente_id': cliente_id,
            'razao_social': razao_social,
        })

cursor = conn.cursor()

for transaction in transactions:
    cursor.execute(
        "INSERT INTO conciliacao.tb_valores (data, movimentacao, debito, credito, n_doc, bank_id, account_id, cliente_id, razao_social) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)",
        (
            transaction['data'],
            transaction['movimentacao'],
            transaction['debito'],
            transaction['credito'],
            transaction['n_doc'],
            transaction['bank_id'],
            transaction['account_id'].replace('-',''),
            transaction['cliente_id'],
            transaction['razao_social'],
        )
    )

conn.commit()
cursor.close()
conn.close()