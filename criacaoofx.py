from ofxparse import OfxParser

nome_arquivo_ofx = 'BB.ofx'

with open(nome_arquivo_ofx, 'rb') as bb_ofx:
    ofx = OfxParser.parse(bb_ofx)

transactions = []

cliente_id = input("Digite o id do cliente:")
razao_social = input("Digite a razão social:")

for account in ofx.accounts:
    for transaction in account.statement.transactions:
        if transaction.amount < 0:
            debito = abs(transaction.amount)
            credito = 0.00
        else:
            debito = 0.00
            credito = max(transaction.amount, 0)
        transactions.append({
            'data': transaction.date.date(),
            'movimentacao': transaction.memo,
            'debito': debito,
            'credito': credito,
            'n_doc': transaction.checknum,
            'banco': account.institution,
            'bank_id': account.routing_number,
            'account_id': account.account_id,
            'cliente_id': cliente_id,
            'razao_social': razao_social,
        })
